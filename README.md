# Decision Trees for Stock Price Prediction

This README offers a comprehensive overview of the project, encompassing setup guidelines, advanced performance tuning methods, and the repository's structural organization.

[[_TOC_]]

## Team
![Alt text](img/DSC05522.JPG){width=240 height=160px}

**Yadvesh Yadav**

Email: [yyada@illinois.edu](mailto:yyada@illinois.edu)  
LinkedIn: https://www.linkedin.com/in/yadvesh/

As a Master's student in Financial Mathematics at the University of Illinois Urbana-Champaign, I have honed my skills in financial data analysis and data science, supported by a strong background in Data Science from my work experience as a Data Science Engineer at Nagarro Softwares in India. I have developed predictive models and managed large datasets to support strategic decisions in the banking sector. I am passionate about applying my mathematical and computational knowledge to develop innovative solutions in the financial industry, particularly through advanced trading algorithms and data analysis techniques. My goal is to leverage my interdisciplinary expertise to drive forward the capabilities of quantitative trading systems and contribute to the cutting edge of financial technology.

# Project Summary

## Objective
The objective of this project is to predict the stock price movement using previous Bid-Ask and Trade Prices. There exists a correlation between the current Bid-Ask spreads and the Mid-point Prices with the current traded price of a stock. These combined features, when processed on a rolling basis, can be used to predict future stock price movements. Apple stock was chosen for these predictions.

## What Are Decision Trees?
Decision Trees are a type of supervised learning algorithm used for both classification and regression tasks. They work by splitting the data into subsets based on the value of input features, which form a tree-like model of decisions. Each internal node of the tree represents a decision based on a feature, each branch represents the outcome of that decision, and each leaf node represents a predicted value or class. The algorithm aims to create a model that predicts the target variable by learning simple decision rules inferred from the data features. Decision Trees are popular due to their simplicity, interpretability, and ability to handle both numerical and categorical data.

## How Decision Trees Work:

1. **Splitting the Data:**
At each node, the algorithm selects the best feature and threshold that splits the data into two subsets, aiming to increase the homogeneity of the target variable within each subset. For regression tasks, this often means minimizing the variance within each subset.

For a given split at node $(t)$, with subsets $(t_L)$ and $(t_R):$

$$\[\text{Variance}(t) = \frac{1}{N_t}\sum_{i \in t}(y_i-\bar{y}_t)^2\]$$

where $(N_t)$ is the number of observations at node $(t),(y_i)$ is the target value for observation $(i)$, and $(\bar{y}_t)$ is the mean target value at node $(t).$

2. **Mean Squared Error (MSE):**
To evaluate the quality of a split, decision trees for regression typically use the Mean Squared Error (MSE) as a criterion. The MSE for a split is calculated as:

$$\[\text{MSE} = \frac{1}{N}\sum_{i=1}^{N}(y_i-\hat{y})^2\]$$

where $(N)$ is the number of observations, $(y_i)$ is the actual target value, and $(\hat{y})$ is the predicted target value (mean of the target values in the node).

3. **Information Gain:**
The algorithm evaluates each possible split by calculating the reduction in impurity (variance in the case of regression) and selects the split that maximizes this reduction. The reduction in variance can be seen as a measure of information gain.

4. **Leaf Nodes:**
The process continues recursively, creating a branch for each possible outcome of the split until a stopping criterion is met (e.g., a maximum tree depth or a minimum number of samples per leaf). The leaf nodes represent the final predictions, which are usually the mean target value of the samples in that leaf.

## Pruning the Tree:
Pruning is a technique used to reduce the size of a decision tree to prevent overfitting. This can be done in two main ways:

1. **Pre-pruning (Early Stopping):**
   This involves stopping the tree-growing process early based on certain conditions, such as a maximum depth of the tree, a minimum number of samples required to split a node, or a minimum impurity decrease required to make a split.

2. **Post-pruning:**
   This involves growing a full tree first and then removing branches that have little importance. Post-pruning can be done using methods like cost complexity pruning, where branches are pruned if they result in a small increase in the tree’s overall error rate.

## Methodolgy
### Data Collection:
To collect data, the IEX parser provided by Prof. Lariviere was deployed in the UIUC campus cluster to handle large datasets. The data was collected from 1st June 2023 till 29th December 2023. Since the data downloaded is of individual dates, a shell script was used to unzip and merge the order book and the trade book, respectively. This provided a consolidated dataset that was ready for further processing.

### Data Imputation:
Handling missing values is crucial for maintaining the integrity of the dataset. A KNN (K-Nearest Neighbors) imputer was used for this purpose. The imputer scales the features, applies a KNN imputation with 5 nearest neighbors, and then rescales the data to obtain clean, imputed data. This process ensures that the dataset remains comprehensive and accurate.

### Feature Engineering:
Feature engineering was performed to extract crucial features that could influence the stock price prediction. The following features were extracted:

* Mid-Prices: Calculated as the average of the Bid and Ask prices.
* Bid-Ask Spreads: The difference between the Bid and Ask prices.
* Relative Strength Index (RSI): A momentum oscillator that measures the speed and change of price movements.
* Moving Average Convergence Divergence (MACD) & MACD Signal: Indicators used to identify changes in the strength, direction, momentum, and duration of a trend.
* Exponential Moving Averages (EMAs): EMAs over different periods (5, 20) to smooth out price data and identify trends.

### Feature Selection:
To select the most relevant features for the model, a correlation matrix and Variance Inflation Factor (VIF) analysis were conducted to check for multicollinearity. The selected features included:

* 'MID_PRICE_1', 'MID_PRICE_2', 'MID_PRICE_3'
* 'EMA5', 'EMA20'
* 'RSI'
* 'MACD', 'MACD_signal'
* The trading price of the stock itself ('PRICE_mean')

### Training, Validation and Testing Set
The entire dataset was split into 60% for training, 20% for validation, and 20% for testing. These datasets were created using a sliding window of 30 intervals. For example, the first 30 rows were used as training data for predicting the 31st trade price, the 1st to 31st rows were used to predict the 32nd trade price, and so on. This method ensures that the model is trained and validated on a rolling basis, which is crucial for time-series forecasting.

### Decision Tree Regressor
A Decision Tree Regressor was implemented as the initial model. A grid search with 5-fold cross-validation was performed for hyperparameter tuning. The best regressor was selected based on the cross-validation results to predict the price and movement of Apple stock. The prediction results indicated a certain level of accuracy and reliability in the model's ability to forecast stock prices.

![Alt text](img/result.jpg){width=1200 height=800px}

### Random Forest Regressor
A Random Forest is an ensemble learning method that builds multiple decision trees and merges them together to get a more accurate and stable prediction. Each tree in the forest is trained on a random subset of the data and makes a prediction independently. The final prediction of the Random Forest is the average (in regression tasks) or the majority vote (in classification tasks) of the predictions of all the individual trees. This method reduces the risk of overfitting, which is a common problem with decision trees, by averaging out the predictions. Random Forests are known for their robustness, accuracy, and ability to handle a large number of features. Despite its advantages, the implementation of Random Forests can be computationally intensive, especially with large datasets. In this project, the initial attempts to implement a RandomForest regressor were hindered by the lack of sufficient computing power. However, future steps should include leveraging the UIUC campus cluster to handle these memory-intensive tasks, thereby allowing for the deployment of Random Forest models.

### Gradient Boosting Decision Trees
Gradient Boosting Decision Trees (GBDT) is another powerful ensemble method that builds trees sequentially. Each tree is built to correct the errors made by the previous trees, with the goal of reducing the overall error of the model. Unlike Random Forests, where trees are built independently, in Gradient Boosting, each new tree is added to the ensemble to improve the performance of the existing trees. This is done by minimizing a loss function, which measures how well the model is performing. GBDT is highly effective and can lead to high predictive performance. It is particularly useful in scenarios where the relationship between the features and the target variable is complex and nonlinear. However, the sequential nature of the algorithm makes it more computationally expensive and slower to train compared to Random Forests.

In this project, the implementation of Gradient Boosting regressors faced similar computational challenges as the Random Forest regressor. To overcome these limitations, future work should include setting up a CI/CD pipeline on the campus cluster to efficiently manage these resource-intensive tasks. By doing so, advanced ensemble methods like GBDT can be effectively utilized to enhance the accuracy of stock price predictions.

## Future Steps
Although initial attempts to implement RandomForest and GradientBoost regressors were made, the lack of sufficient computing power hindered these efforts. To further this project:

* Implementing a CI/CD pipeline over the campus cluster can handle memory-intensive tasks more efficiently.
* Exploring advanced ensemble methods could capture the data more accurately and improve prediction performance.
* Additionally, incorporating more sophisticated machine learning models and optimizing computational resources would be beneficial.

By expanding computational capabilities and refining the model, more accurate and reliable stock price predictions can be achieved, paving the way for robust trading strategies based on machine learning insights.

