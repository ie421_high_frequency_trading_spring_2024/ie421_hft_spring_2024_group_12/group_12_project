#!/bin/bash

# Define the directory where book update files are located
BOOK_UPDATES_DIR="book_snapshots"

# Define the input and output file
input_file="${BOOK_UPDATES_DIR}/20230601_book_updates.csv"
output_file="${BOOK_UPDATES_DIR}/processed_book.csv"

# Capture the header
HEADER=$(head -n 1 "$input_file")

# Process the file
awk -F, -v header="$HEADER" 'BEGIN {
    OFS=",";
    print header;  # Print the header stored from the shell
}
NR>1 {
    # Convert the time from UTC to Eastern Time considering DST
    cmd="TZ=\"America/New_York\" date -u -d \"" $1 " UTC\" +\"%Y-%m-%d %H:%M:%S.%N\""
    cmd | getline new_time
    close(cmd)
    $1 = new_time
    print $0
}' "$input_file" > "$output_file"

