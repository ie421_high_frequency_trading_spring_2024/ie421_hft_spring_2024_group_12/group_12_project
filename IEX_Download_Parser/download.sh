#!/bin/bash

python3 src/download_iex_pcaps.py --start-date 2023-06-01 --end-date 2023-08-31 --download-dir data/iex_downloads
