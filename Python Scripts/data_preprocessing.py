#!/usr/bin/env python
# coding: utf-8

# importing necessary libraries
import pandas as pd
import pytz


# setting the file path and data_dict
file_path = "../book_snapshots/merged_book.csv"

datatype_dict = {'COLLECTION_TIME': object ,
                 'MESSAGE_ID': int,
                 'MESSAGE_TYPE': object,
                 'SYMBOL': object,
                 'BID_PRICE_1': float,
                 'BID_SIZE_1': int,
                 'BID_PRICE_2': float,
                 'BID_SIZE_2': int,
                 'BID_PRICE_3': float,
                 'BID_SIZE_3': int,
                 'ASK_PRICE_1': float,
                 'ASK_SIZE_1': int,
                 'ASK_PRICE_2': float,
                 'ASK_SIZE_2': int,
                 'ASK_PRICE_3': float,
                 'ASK_SIZE_3': int}


# reading the order book data
df_o = pd.read_csv(file_path, dtype=datatype_dict)

# converting the Collection Time to datetime
df_o['COLLECTION_TIME'] = pd.to_datetime(df_o['COLLECTION_TIME'])

# filtering data for your specific ticker # i chose APPL (choose yours)
df_oappl = df_o.loc[df_o['SYMBOL']=='AAPL']

# grouping by 'COLLECTION_TIME' and aggregating with mean so that each Collection time timestamp has a common value
# did this so that there are no duplicates while resampling in the next step
df_oappl_agg = df_oappl.groupby('COLLECTION_TIME').agg({
    'BID_PRICE_1': 'mean',
    'ASK_PRICE_1': 'mean',
    'BID_PRICE_2': 'mean',
    'ASK_PRICE_2': 'mean',
    'BID_PRICE_3': 'mean',
    'ASK_PRICE_3': 'mean'
})

# resampling the entire DataFrame to 1-minute intervals using mean aggregation to get the mean values in that 1-minute interval
df_oappl_resampled = df_oappl_agg.resample('1T').agg({
    'BID_PRICE_1': 'mean',
    'ASK_PRICE_1': 'mean',
    'BID_PRICE_2': 'mean',
    'ASK_PRICE_2': 'mean',
    'BID_PRICE_3': 'mean',
    'ASK_PRICE_3': 'mean'
})


# resetting index to make 'COLLECTION_TIME' a column again
df_oappl_resampled.reset_index(inplace=True)
df_oappl_resampled.head()


# while resampling and aggregating the SYMBOL column was dropped so adding it back (#use your stock symbol here)
df_oappl_resampled['SYMBOL'] = 'AAPL'

# restructuring the dataframe in a good format
df_oappl_resampled = df_oappl_resampled[['COLLECTION_TIME', 'SYMBOL', 'BID_PRICE_1', 'ASK_PRICE_1', 'BID_PRICE_2',
                             'ASK_PRICE_2', 'BID_PRICE_3', 'ASK_PRICE_3']]


# defining a function to convert Collection times from UTC to Eastern(New York) time
def convert_utc_to_eastern(utc_datetime):
    utc_timezone = pytz.timezone("UTC")
    eastern_timezone = pytz.timezone("America/New_York")
    eastern_datetime = utc_timezone.localize(utc_datetime).astimezone(eastern_timezone)
    return eastern_datetime.strftime('%Y-%m-%d %H:%M:%S')

# applying the conversion function to the COLLECTION_TIME column
df_oappl_resampled['COLLECTION_TIME'] = df_oappl_resampled['COLLECTION_TIME'].apply(convert_utc_to_eastern)

# reading the trade book data
file_path1 = "../book_snapshots/merged_trade_files.csv"
df_t = pd.read_csv(file_path1)

# converting the Collection Time to datetime
df_t['COLLECTION_TIME'] = pd.to_datetime(df_t['COLLECTION_TIME'])

# filtering the trade book with the symbol chosen (# choose yours)
df_tappl = df_t.loc[df_t['SYMBOL']=='AAPL']
df_tappl.head()

# filter the trade book dataFrame for rows where 'TRADE_FLAGS' contains 'REGULAR_HOURS'
df_tappl_filter = df_tappl[df_tappl['TRADE_FLAGS'].str.contains('REGULAR_HOURS')]

# grouping by 'COLLECTION_TIME' and aggregating with specific functions so that there is no duplicacy in timestamps while resampling
df_tappl_agg = df_tappl_filter.groupby('COLLECTION_TIME').agg({
    'PRICE': 'mean',
    'SIZE': 'sum'
})

# resample the entire dataFrame to 1-minute intervals using nearest
df_tappl_resampled = df_tappl_agg.resample('1T').agg({
    'PRICE': ['mean', 'max', 'min'],
    'SIZE': 'sum'
})

# resetting indexing due to multi-level index after resampling
df_tappl_resampled.columns = ['_'.join(col).strip() for col in df_tappl_resampled.columns.values]

# reset index to make 'COLLECTION_TIME' a column again
df_tappl_resampled.reset_index(inplace=True)

# applying the conversion function to the COLLECTION_TIME column to convert it from UTC to Eastern
df_tappl_resampled['COLLECTION_TIME'] = df_tappl_resampled['COLLECTION_TIME'].apply(convert_utc_to_eastern)

# printing length of both trade book and order book to check for consistency
print(len(df_tappl_resampled))
print(len(df_oappl_resampled))

# merging the trade book and order book to get prices and size in every 1 minute interval
order_trade_merged = pd.merge(df_oappl_resampled, df_tappl_resampled, on='COLLECTION_TIME', how='left')
order_trade_merged['COLLECTION_TIME'] = pd.to_datetime(order_trade_merged['COLLECTION_TIME'])
order_trade_merged.set_index('COLLECTION_TIME', inplace=True)

# exporting the merged data to a csv
order_trade_merged.to_csv("../analysis_book/order_trade_merged.csv", index=True)