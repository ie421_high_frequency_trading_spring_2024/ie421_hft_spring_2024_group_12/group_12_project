import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import RobustScaler
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import GridSearchCV

# reading the imputed and engineered data to fit to the regression model
final_df = pd.read_csv("../../analysis_book/final_df.csv", index_col=0)

## fitting a  decision tree regression model to predict continuously
# normalizing the features to fit into the decision tree
scaler = RobustScaler()
feature_use = final_df.drop(['COLLECTION_TIME', 'SYMBOL'], axis=1).values
df_normalized = scaler.fit_transform(feature_use)

# defining the size of the training data (60% of the dataset)
train_len = int(len(df_normalized) * 0.6)

# splitting remaining data into validation and test data (50% each)
val_test_split_index = train_len + int((len(df_normalized) - train_len) * 0.5)

# define a sliding window length
window_length = 30

# function to create training, validation and testing datasets with sliding window
def create_dataset(data, start_index, end_index, window_length):
    x_data, y_data = [], []
    for i in range(start_index + window_length, end_index + 1):
        x_data.append(data[i - window_length:i, :])  # Using all features including 'PRICE_mean'
        y_data.append(data[i, -1])  # Assuming the last column is 'PRICE_mean'
    return np.array(x_data), np.array(y_data)

# preparing training, validation, and test data
x_train, y_train = create_dataset(df_normalized, 0, train_len - 1, window_length)
x_val, y_val = create_dataset(df_normalized, train_len, val_test_split_index - 1, window_length)
x_test, y_test = create_dataset(df_normalized, val_test_split_index, len(df_normalized) - 1, window_length)

# flattening the data for Decision Tree Regressor
x_train_tree = x_train.reshape(x_train.shape[0], -1)
x_val_tree = x_val.reshape(x_val.shape[0], -1)
x_test_tree = x_test.reshape(x_test.shape[0], -1)

# printing the shape of trees to check for consistency
print(f'x_train_tree shape: {x_train_tree.shape}')
print(f'x_train_tree shape: {x_val_tree.shape}')
print(f'x_test_tree shape: {x_test_tree.shape}')

# defining a decision tree regressor
regressor = DecisionTreeRegressor(random_state=3296)

# defining a parameter grid for grid search CV
param_grid = {
    'max_depth': [None, 10, 20, 30],
    'min_samples_split': [2, 3, 4, 5, 7, 10, 20, 30],
    'min_samples_leaf': [1, 2, 3, 4, 5, 7, 10, 20],
    'ccp_alpha': [0.0, 0.000001, 0.00001, 0.0001, 0.001, 0.1, 1]
}

# grid search CV setup
grid_search = GridSearchCV(regressor, param_grid, cv=5, scoring='neg_mean_squared_error', n_jobs=-1)


# fitting the training data for grid search
grid_search.fit(x_train_tree, y_train)


# best parameters from the grid search
print("Best parameters from Grid Search:", grid_search.best_params_)
print("Best score from Grid Search:", grid_search.best_score_)

# best estimator
best_regressor = grid_search.best_estimator_

# validation set and testing set predictions
y_val_pred = best_regressor.predict(x_val_tree)
y_test_pred = best_regressor.predict(x_test_tree)

# validation set and testing set MSE's
val_mse = mean_squared_error(y_val, y_val_pred)
print(f'Validation MSE: {val_mse}')
test_mse = mean_squared_error(y_test, y_test_pred)
print(f'Test MSE: {test_mse}')

# printing the important features of the model
importances = best_regressor.feature_importances_
print(importances)

# rescaling back the data for visualization
y_pred_reshaped = y_test_pred.reshape(-1, 1)
y_test_reshaped = y_test.reshape(-1, 1)

# creating an array for inverse transformation
transformed_shape = np.zeros((len(y_pred_reshaped), df_normalized.shape[1]))
transformed_shape[:, -1] = y_pred_reshaped[:, 0] 

# inverse transforming predictions
y_pred_actual = scaler.inverse_transform(transformed_shape)[:, -1]

# inverse transforming y_test
transformed_shape[:, -1] = y_test_reshaped[:, 0]
y_test_actual = scaler.inverse_transform(transformed_shape)[:, -1]


# craeting a dataframe from y_test and y_test_pred to plot
test_data_rescaled = pd.DataFrame({
    'Actual': y_test_actual,
    'Predicted': y_pred_actual
})

# resetting the index
test_data_rescaled.reset_index(inplace=True, drop=True)

# plotting the results
plt.figure(figsize=(18, 10))
plt.plot(test_data_rescaled['Actual'], label='Actual Values', color='blue')
plt.plot(test_data_rescaled['Predicted'], label='Predicted Values', color='orange', alpha=0.7)
plt.title('Comparison of Actual and Predicted Values')
plt.xlabel('Time (arbitrary units)')
plt.ylabel('Stock Price Mean')
plt.legend()

# saving the plot for decision tree 
plt.savefig('../../plots/decision_tree_pred.jpg', format='jpg', dpi=300)