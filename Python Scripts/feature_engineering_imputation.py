
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.impute import KNNImputer

########################################## Feature Engineering ##################################################

# reading the analysis dataframe
order_trade_merged = pd.read_csv("../analysis_book/order_trade_merged.csv", index_col=0)

# calculating the mid_prices upto 3 levels
for i in range(1, 4):  
    order_trade_merged[f'MID_PRICE_{i}'] = order_trade_merged[[f'BID_PRICE_{i}', f'ASK_PRICE_{i}']].mean(axis=1)

# calculating the bid-ask spreads upto 3 levels
for i in range(1, 4):  
    order_trade_merged[f'spread_{i}'] = order_trade_merged[f'ASK_PRICE_{i}'] - order_trade_merged[f'BID_PRICE_{i}']

# defining a function to calculate RSI(Relative Strength Index)
def RSI(series, period=10):
    delta = series.diff(1)
    gain = (delta.where(delta > 0, 0)).rolling(window=period).mean()
    loss = (-delta.where(delta < 0, 0)).rolling(window=period).mean()
    RS = gain / loss
    return 100 - (100 / (1 + RS))

# defining a function to calculate the Moving Average Convergence Divergence(MACD)
def MACD(series, slow=26, fast=12):
    exp1 = series.ewm(span=fast, adjust=False).mean()
    exp2 = series.ewm(span=slow, adjust=False).mean()
    macd = exp1 - exp2
    signal = macd.ewm(span=9, adjust=False).mean() 
    return macd, signal

# defining a function to calculate the Exponential Moving Average(EMA)
def EMA(series, period=20):
    return series.ewm(span=period, adjust=False).mean()

# calculating RSI for tradeing price (PRICE_mean)
order_trade_merged['RSI'] = RSI(order_trade_merged['PRICE_mean'])

# calculating MACD and MACD signal for trading price (PRICE_mean)
order_trade_merged['MACD'], order_trade_merged['MACD_signal'] = MACD(order_trade_merged['PRICE_mean'])

# calculating EMA for trading price (PRICE_mean)
order_trade_merged['EMA5'] = EMA(order_trade_merged['PRICE_mean'], period=5)
order_trade_merged['EMA20'] = EMA(order_trade_merged['PRICE_mean'], period=20)
#######################################################################################################################

############################################ Data Imputation ##########################################################

# printing all the Null values in every column of merged data
print(order_trade_merged.isna().sum())

# I have used a KNN imputer here 
# scaling the data for imputation
data = order_trade_merged.copy()
data.reset_index(inplace=True)
non_numeric_columns = ['COLLECTION_TIME', 'SYMBOL']
non_numeric_data = data[non_numeric_columns]
numeric_data = data.drop(columns=non_numeric_columns)
scaler = StandardScaler()
scaled_data = scaler.fit_transform(numeric_data)

# imputing the missing values
imputer = KNNImputer(n_neighbors=5)
imputed_scaled_data = imputer.fit_transform(scaled_data)

# rescaling data back to original scale
rescaled_data = scaler.inverse_transform(imputed_scaled_data)
final_numeric_data = pd.DataFrame(rescaled_data, columns=numeric_data.columns)

# the final data after imputation
final_data = pd.concat([non_numeric_data, final_numeric_data], axis=1)

# saving the imputed data as order_trade_imputed and setting Collection time as index
order_trade_imputed = final_data.copy()
order_trade_imputed.set_index('COLLECTION_TIME', inplace=True)
order_trade_imputed.head()

# reordering the imputed data 
order_trade_imputed = order_trade_imputed[['SYMBOL', 'BID_PRICE_1', 'ASK_PRICE_1', 'BID_PRICE_2', 'ASK_PRICE_2',
                                           'BID_PRICE_3', 'ASK_PRICE_3', 'PRICE_max', 'PRICE_min',
                                           'SIZE_sum', 'MID_PRICE_1', 'MID_PRICE_2', 'MID_PRICE_3', 'spread_1', 'spread_2', 'spread_3', 'RSI',
                                           'MACD', 'MACD_signal', 'EMA5', 'EMA20', 'PRICE_mean']]

# extracting the final_df on which the models will be implemented
final_df = order_trade_imputed[['SYMBOL', 'MID_PRICE_1', 'MID_PRICE_2', 'MID_PRICE_3', 'EMA5', 'EMA20', 'RSI', 'MACD', 'MACD_signal','PRICE_mean']]
final_df.reset_index(inplace=True)
final_df = final_df.dropna()

# exporting the imputed and engineered data to a csv
final_df.to_csv("../analysis_book/final_df.csv", index=True)