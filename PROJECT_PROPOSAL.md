# Project Name: ML Trading using Decision Trees (Group 12)

## Introduction
This project aims to explore the application of decision tree algorithms in predicting stock market trends. By analyzing historical market data, the project seeks to develop models that can identify patterns and signals preceding significant market movements. The goal is to evaluate the effectiveness of decision tree-based trading strategies in terms of accuracy and profitability compared to traditional trading methods.

## Technology Stack

I plan to use the following technologies, libraries, and languages:

- **Python**: For its extensive libraries and support in data analysis and machine learning.
- **Scikit-learn**: To implement decision tree algorithms efficiently.
- **Pandas** and **NumPy**: For data manipulation and numerical calculations.
- **Matplotlib** and **Seaborn**: For data visualization.
- **Jupyter Notebook**: For interactive development and data exploration.
- **C++**: To build high-performance components of the trading algorithm, leveraging its speed and efficiency. C++ is well-suited for time-sensitive financial computations where performance is critical.
- **C++ Standard Library** and **Boost**: For advanced mathematical computations and algorithms in C++.

## Hardware Requirements

The project will primarily run on high-performance computing resources to handle large datasets efficiently. I will use:

- Local machines with at least 16GB RAM for initial development and backtesting.
- The university's computer cluster for high-performance computing tasks and scale the trading algorithm.
- Lab machines provided by Professor David Lariviere.

These resources will ensure that we have the necessary computing power and memory to process large volumes of data and perform complex analyses.

## Project Timeline

- **Week 1**: Data collection and initial analysis.
- **Week 2-3**: Development of decision tree models and backtesting.
- **Week 4**: Implementation of Model in C++ and Optimization of model parameters and configurations.
- **Week 5**: Integration with the backend and development of GUI to visualize real-time PnL of trading startegies.
- **Week 6**: Final testing, bug fixing, and project presentation.

## Goals and Objectives

- **Minimum Target**: Successfully predict stock market trends with a decision tree model in Python.
- **Expected Completion**: Develop a fully functioning system optimized in C++ that outperforms traditional trading methods in backtesting.
- **Reach Goals**: Implement additional features like real-time data processing and automated trading capabilities along with a GUI to monitor PnL from the trading strategy.

## Team Contributions

- Yadvesh (Lone Wolf): Will implement the project end-to-end.


## External Resources

We will need access to historical and real-time market data from financial databases like Yahoo Finance or Google Finance and Professor David's proprietary stock data as json files. Additional resources may include access to financial analytics platforms such as strategy studio.

## Final Deliverables

The project will provide real-time market trend predictions, complete with a backend system processing historical market data using decision tree models.

## Team Information

- Yadvesh: Expected to graduate in Spring 2025. Plans to pursue a Career in Quantitative analysis of Market Trends.
