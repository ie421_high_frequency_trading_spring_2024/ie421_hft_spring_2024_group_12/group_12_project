#!/bin/bash

# Define the directory where book update files are located
BOOK_UPDATES_DIR="book_snapshots"

# Full path for the merged output file
OUTPUT_FILE1="$BOOK_UPDATES_DIR/merged_book.csv"
OUTPUT_FILE2="$BOOK_UPDATES_DIR/merged_trade_files.csv"

# Check if the merged book file already exists and remove it if it does
if [ -f "$OUTPUT_FILE1" ]; then
    echo "Removing existing $OUTPUT_FILE1"
    rm -f "$OUTPUT_FILE1"
fi

# Initialize the output file and add the header from the first file
head -n 1 "$BOOK_UPDATES_DIR/$(ls $BOOK_UPDATES_DIR | grep '_book_updates.csv' | head -n 1)" > "$OUTPUT_FILE1"

# Loop through all files that match the pattern *_book_updates.csv in the specified directory
for file in $BOOK_UPDATES_DIR/*_book_updates.csv
do
    echo "Appending $file to $OUTPUT_FILE1"
    # Append the file content except the header
    tail -n +2 $file >> "$OUTPUT_FILE1"
done

echo "All book update files have been concatenated into $OUTPUT_FILE1"


# Check if the merged trade file already exists and remove it if it does
if [ -f "$OUTPUT_FILE2" ]; then
    echo "Removing existing $OUTPUT_FILE2"
    rm -f "$OUTPUT_FILE2"
fi

# Initialize the output file and add the header from the first file
head -n 1 "$BOOK_UPDATES_DIR/$(ls $BOOK_UPDATES_DIR | grep '_trades.csv' | head -n 1)" > "$OUTPUT_FILE2"

# Loop through all files that match the pattern *_trades.csv in the specified directory
for file1 in $BOOK_UPDATES_DIR/*_trades.csv
do
    echo "Appending $file1 to $OUTPUT_FILE2"
    # Append the file content except the header
    tail -n +2 $file1 >> "$OUTPUT_FILE2"
done

echo "All trade update files have been concatenated into $OUTPUT_FILE2"
